package com.miouyouyou.libraries.helpers;

import java.io.Writer;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.IOException;

/** This class provide multiple helper methods to manipulate exceptions.
 */
public class JavaExceptionHelpers {
  /** Return the stacktrace displayed by Throwable#printStackTrace(PrintWriter)
   *  as a String.
   * @param throwable  The throwable to extract the default displayed 
   *                   stacktrace from.
   * @return the stacktrace of the provided throwable, as it would be displayed
   *         by Throwable#printStackTrace(PrintWriter).
   * @throws RuntimeException  This can occur if, internally, printStackTrace 
   *                           throws a java.io.IOException when the provided
   *                           PrintWriter tries to write in the provided 
   *                           Writer. See java.io.Writer#write() for details.
   */
  public static String get_stacktrace_from(Throwable throwable) {
    Writer print_output = new StringWriter();
    try {
      throwable.printStackTrace(new PrintWriter(print_output));
      print_output.flush();
    }
    catch (IOException e) { throw new RuntimeException(e); }
    return print_output.toString();
  }
}
