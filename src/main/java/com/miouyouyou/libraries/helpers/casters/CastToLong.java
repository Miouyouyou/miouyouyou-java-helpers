package com.miouyouyou.libraries.helpers.casters;
import com.miouyouyou.libraries.helpers.AbstractPrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.JavaCastHelpers;

/** The purpose of this class is to cast any primitive value, boxed or
 *  unboxed, to Long.
 * This is done by providing three static subclasses :
 * - Standard, which cast the same way the JVM does;
 * - BoundToMinMax, which cast while setting the casted value to the
 *   minimum and maximum values representable its type, if a negative
 *   or positive overflow were to occur;
 * - ThrowOnOverflow, which will throw an overflow if the cast provoke
 *   an overflow.
 * These subclasses, like the other subclasses of CastTo classes,
 * implements the PrimitiveBoxedCaster interface, allowing to cast
 * primitive values and objects through a pre-determined method.
 * You might want to use QuickBoxedCaster if you want to cast values
 * dynamically (i.e. to primitive classes determined at run-time).
 */
public class CastToLong {
  /** This subclass methods cast values through a simple cast, providing the
   *  value that the JVM would provide, which mean no overflow check for 
   *  integer overflows.
   */
  public static class Standard extends AbstractPrimitiveBoxedCaster {
    /** Cast the provided value to Long.
     * @param value  the value to cast
     * @return the value casted to Long
     */
    public Object cast(long   value) { return value; }

    /** Cast the provided value to Long.
     * @param value  the value to cast
     * @return the value casted to Long
     */
    public Object cast(double value) { return (long) value; }
  }

  /** This subclass methods cast values, and set them to the nearest maximal
   *  value that can be represented by this class if it were to overflow.
   *  That is :
   *  -  Long.MAX_VALUE for positive overflows;
   *  - Long.MIN_VALUE for negative overflows.
   */
  public static class BoundToMinMax extends AbstractPrimitiveBoxedCaster {
    /** Cast the provied value to Long, setting
     *  it to :
     *  -  Long.MAX_VALUE for positive overflows:
     *  - Long.MIN_VALUE for negative overflows.
     * @param value  the value to cast
     * @return the value casted to Long and unchanged if it didn't
     *         overflow, Long.MAX_VALUE in case of positive overflows,
     *         Long.MIN_VALUE in case of negative overflows.
     */
    public Object cast(long value) {
      return JavaCastHelpers.to_long_bounded_to_minmax(value);
    }

    /** Cast the provied value to Long, setting
     *  it to :
     *  -  Long.MAX_VALUE for positive overflows:
     *  - Long.MIN_VALUE for negative overflows.
     * @param value  the value to cast
     * @return the value casted to Long and unchanged if it didn't
     *         overflow, Long.MAX_VALUE in case of positive overflows,
     *         Long.MIN_VALUE in case of negative overflows.
     */
    public Object cast(double value) {
      return JavaCastHelpers.to_long_bounded_to_minmax(value);
    }
  }

  /** This subclass methods will cast the value or throw a NumberFormatException
   *  if the cast would provoke an overflow.
   */
  public static class ThrowOnOverflow extends AbstractPrimitiveBoxedCaster {
    /** Cast the value to Long or throw a
     *  NumberFormatException if the cast would provoke an overflow.
     * @param value  the value to cast
     * @return the value casted to Long.
     * @throws NumberFormatException if the cast would provoke an overflow.
     */
    public Object cast(long value) {
      return JavaCastHelpers.to_long_throw_on_overflow(value);
    }

    /** Cast the value to Long or throw a
     *  NumberFormatException if the cast would provoke an overflow.
     * @param value  the value to cast
     * @return the value casted to Long.
     * @throws NumberFormatException if the cast would provoke an overflow.
     */
    public Object cast(double value) {
      return JavaCastHelpers.to_long_throw_on_overflow(value);
    }
  }

}
