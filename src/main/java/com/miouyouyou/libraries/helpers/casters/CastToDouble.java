package com.miouyouyou.libraries.helpers.casters;
import com.miouyouyou.libraries.helpers.AbstractPrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.JavaCastHelpers;

/** The purpose of this class is to cast any primitive value, boxed or
 *  unboxed, to Double.
 * This is done by providing three static subclasses :
 * - Standard, which cast the same way the JVM does;
 * - BoundToMinMax, which cast while setting the casted value to the
 *   minimum and maximum values representable its type, if a negative
 *   or positive overflow were to occur;
 * - ThrowOnOverflow, which will throw an overflow if the cast provoke
 *   an overflow.
 * These subclasses, like the other subclasses of CastTo classes,
 * implements the PrimitiveBoxedCaster interface, allowing to cast
 * primitive values and objects through a pre-determined method.
 * You might want to use QuickBoxedCaster if you want to cast values
 * dynamically (i.e. to primitive classes determined at run-time).
 */
public class CastToDouble {
  /** This subclass methods cast values through a simple cast, providing the
   *  value that the JVM would provide, which mean no overflow check for 
   *  integer overflows.
   */
  public static class Standard extends AbstractPrimitiveBoxedCaster {
    /** Cast the provided value to Double.
     * @param value  the value to cast
     * @return the value casted to Double
     */
    public Object cast(long   value) { return (double) value; }

    /** Cast the provided value to Double.
     * @param value  the value to cast
     * @return the value casted to Double
     */
    public Object cast(double value) { return value; }
  }

  /** This subclass methods cast values, and set them to the nearest maximal
   *  value that can be represented by this class if it were to overflow.
   *  That is :
   *  -  Double.MAX_VALUE for positive overflows;
   *  - -Double.MAX_VALUE for negative overflows.
   */
  public static class BoundToMinMax extends AbstractPrimitiveBoxedCaster {
    /** Cast the provied value to Double, setting
     *  it to :
     *  -  Double.MAX_VALUE for positive overflows:
     *  - -Double.MAX_VALUE for negative overflows.
     * @param value  the value to cast
     * @return the value casted to Double and unchanged if it didn't
     *         overflow, Double.MAX_VALUE in case of positive overflows,
     *         -Double.MAX_VALUE in case of negative overflows.
     */
    public Object cast(long value) {
      return JavaCastHelpers.to_double_bounded_to_minmax(value);
    }

    /** Cast the provied value to Double, setting
     *  it to :
     *  -  Double.MAX_VALUE for positive overflows:
     *  - -Double.MAX_VALUE for negative overflows.
     * @param value  the value to cast
     * @return the value casted to Double and unchanged if it didn't
     *         overflow, Double.MAX_VALUE in case of positive overflows,
     *         -Double.MAX_VALUE in case of negative overflows.
     */
    public Object cast(double value) {
      return JavaCastHelpers.to_double_bounded_to_minmax(value);
    }
  }

  /** This subclass methods will cast the value or throw a NumberFormatException
   *  if the cast would provoke an overflow.
   */
  public static class ThrowOnOverflow extends AbstractPrimitiveBoxedCaster {
    /** Cast the value to Double or throw a
     *  NumberFormatException if the cast would provoke an overflow.
     * @param value  the value to cast
     * @return the value casted to Double.
     * @throws NumberFormatException if the cast would provoke an overflow.
     */
    public Object cast(long value) {
      return JavaCastHelpers.to_double_throw_on_overflow(value);
    }

    /** Cast the value to Double or throw a
     *  NumberFormatException if the cast would provoke an overflow.
     * @param value  the value to cast
     * @return the value casted to Double.
     * @throws NumberFormatException if the cast would provoke an overflow.
     */
    public Object cast(double value) {
      return JavaCastHelpers.to_double_throw_on_overflow(value);
    }
  }

}
