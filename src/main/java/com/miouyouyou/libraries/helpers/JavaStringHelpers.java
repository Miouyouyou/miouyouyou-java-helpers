package com.miouyouyou.libraries.helpers;

import java.util.Locale;

/** This class regroup methods dealing with java.lang.String
 */
public class JavaStringHelpers {
  
  /** Generate a concatenation of all the String provided, separated by the 
   *  provided separator.
   * Example :
   * <code>list(',', "Hey", "Hi", "Hello");</code>
   * -> "Hey,Hi,Hello"
   * @param separator  The character that will be inserted between the 
   *                   concatenated elements.
   * @param elements  The concatenated strings.
   * @return a concatenation of all the String provided, separated by the
   *         provided separator.
   * @throws NullPointerException if the separator or the elements are null.
   */
  public static String list(char separator, String... elements) {
    return list(String.format("%c", separator), elements);
  }

  /** Generate a concatenation of all the String provided, separated by the 
   *  provided separator.
   * Example :
   * <code>list(',', "Hey", "Hi", "Hello");</code>
   * -> "Hey,Hi,Hello"
   * @param separator  The Character that will be inserted between the 
   *                   concatenated elements.
   * @param elements  The concatenated strings.
   * @return a concatenation of all the String provided, separated by the
   *         provided separator.
   * @throws NullPointerException if the separator or the elements are null.
   */
  public static String list(Character separator, String... elements) {
    return list(separator.toString(), elements);
  }

  /** Generate a concatenation of all the String provided, separated by the 
   *  provided separator.
   * Example :
   * <code>list(" , ", "Hey", "Hi", "Hello");</code>
   * -> "Hey , Hi , Hello"
   * @param separator  The String that will be inserted between the 
   *                   concatenated elements.
   * @param elements  The concatenated strings.
   * @return a concatenation of all the String provided, separated by the
   *         provided separator.
   * @throws NullPointerException if the separator or the elements are null.
   */
  public static String list(String separator, String... elements) {
    StringBuilder string_builder = new StringBuilder();
    int number_of_strings = elements.length;
    for (int index = 0; index < number_of_strings; index++) {
      string_builder.append(elements[index]);
      if (index < number_of_strings - 1)
        string_builder.append(separator);
    }
    return string_builder.toString();
  }

  /** Indicates if a String is composed of only one Unicode character.
   * @param string  The checked string
   * @return true if the provided string contains only one Unicode character,
   *         false otherwise.
   * @throws NullPointerException  if the argument is null. 
   */
  public static boolean is_a_one_character_string(String string) {
    return (string.length() == 1 &&
            !(Character.isHighSurrogate( string.charAt(0) )) &&
            !(Character.isLowSurrogate( string.charAt(0) )) );
  }

  /** Indicates if an Object is a String composed of only one Unicode character.
   * @param o  The checked string
   * @return true if the provided string contains only one Unicode character,
   *         false otherwise.
   * @throws NullPointerException  if the argument is null. 
   */
  public static boolean is_a_one_character_string(Object o) {
    return ((o instanceof String) && 
            is_a_one_character_string((String) o));
  }

  /** Capitalise the first character of a String, using the English Locale 
   *  to determine how to capitalise a character.
   * @param string  The string to capitalise.
   * @return The provided string capitalised according to English rules of
   *         capitalisation (and the JVM).
   * @throws NullPointerException  if the argument is null.
   */
  public static String capitalise(String string) {
    return capitalise(string, Locale.ENGLISH);
  }

  /** Capitalise the first character of a String, using the provided locale
   *  to determine how to capitalise a character.
   * @param string  The string to capitalise.
   * @param provided_locale  The Locale object determining the capitalisation
   *                         rules to abide to.
   * @return The provided string capitalised according to the rules of the
   *         provided locale (and the JVM).
   * @throws NullPointerException  if any of the arguments is null.
   */
  public static String capitalise(String string, 
                                  Locale provided_locale) {
    return new 
      StringBuilder().
      append(string.substring(0, 1).toUpperCase(provided_locale)).
      append(string.substring(1, string.length())).toString();
  }
}
