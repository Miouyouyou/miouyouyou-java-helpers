package com.miouyouyou.libraries.helpers;

import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.lang.IllegalArgumentException;
import java.lang.reflect.Array;

/** This class contains multiple helper methods focused on classes.
 * Mostly primitive classes, numeric classes and arrays classes.
 */
public class JavaClassHelpers {

  private static Map<Class,Class>  boxed_equivalence;
  private static Map<Class,Class>  primitive_equivalence;
  private static Map<String,Class> primitive_classes_names;
  private static Set<Number> special_floating_point_values;

  static {
    boxed_equivalence = new HashMap<Class,Class>();
    boxed_equivalence.put(void.class,    Void.class);
    boxed_equivalence.put(boolean.class, Boolean.class);
    boxed_equivalence.put(byte.class,    Byte.class);
    boxed_equivalence.put(short.class,   Short.class);
    boxed_equivalence.put(char.class,    Character.class);      
    boxed_equivalence.put(int.class,     Integer.class);
    boxed_equivalence.put(long.class,    Long.class);
    boxed_equivalence.put(float.class,   Float.class);
    boxed_equivalence.put(double.class,  Double.class);
    
    primitive_equivalence = new HashMap<Class,Class>();
    primitive_equivalence.put(Void.class,      void.class);
    primitive_equivalence.put(Boolean.class,   boolean.class);
    primitive_equivalence.put(Byte.class,      byte.class);
    primitive_equivalence.put(Short.class,     short.class);
    primitive_equivalence.put(Character.class, char.class);
    primitive_equivalence.put(Integer.class,   int.class);
    primitive_equivalence.put(Long.class,      long.class);
    primitive_equivalence.put(Float.class,     float.class);
    primitive_equivalence.put(Double.class,    double.class);
    
    primitive_classes_names = new HashMap<String,Class>();
    primitive_classes_names.put("void",    void.class);
    primitive_classes_names.put("boolean", boolean.class);
    primitive_classes_names.put("byte",    byte.class);
    primitive_classes_names.put("short",   short.class);
    primitive_classes_names.put("char",    char.class);
    primitive_classes_names.put("int",     int.class);
    primitive_classes_names.put("long",    long.class);
    primitive_classes_names.put("float",   float.class);
    primitive_classes_names.put("double",  double.class);

    special_floating_point_values = new HashSet<Number>();
    special_floating_point_values.add(Float.NaN);
    special_floating_point_values.add(Float.POSITIVE_INFINITY);
    special_floating_point_values.add(Float.NEGATIVE_INFINITY);
    special_floating_point_values.add(Double.NaN);
    special_floating_point_values.add(Double.POSITIVE_INFINITY);
    special_floating_point_values.add(Double.NEGATIVE_INFINITY);
  }

  /** Returns the boxed equivalent of the primitive class passed as argument.
   * @param primitive_class  The primitive class
   * @return the boxed class of the provided primitive class or null if the 
   *          class provided was not a primitive.
   */
  public static Class get_boxed_equivalent_of(Class primitive_class) {
    return boxed_equivalence.get(primitive_class);
  }

  /** Returns the primitive equivalent of the boxed primitive class.
   *  @param boxed_class  A boxed primitive class.
   *  @return the primitive class represented by the provided boxed class,
   *           or null if the class provided was not a boxed primitive.
   */
  public static Class get_primitive_equivalent_of(Class boxed_class) {
    return primitive_equivalence.get(boxed_class);
  }

  /** Checks if a provided class is a boxed primitive class.
   * @param analysed_class  the class to check
   * @return true if the class is a boxed primitive class.
   */
  public static boolean is_boxed_primitive(Class analysed_class) {
    return (get_primitive_equivalent_of(analysed_class) != null);
  }

  /** Checks if a provided class is a primitive or boxed primitive class.
   * @param analysed_class  the class to check
   * @return true if the class is a primitive or boxed primitive, 
   *         false otherwise
   */
  public static boolean is_primitive(Class analysed_class) {
    return (analysed_class != null && (analysed_class.isPrimitive() ||
                                       is_boxed_primitive(analysed_class)));
  }

  /** Returns the primitive class represented by the provided String.
    * E.g. : "int" returns int.class
    * @param class_name  the name of the primitive class to return
    * @return the primitive class represented by the provided name or 'null' if
    *         no such primitive class exist.
    */
  public static Class primitive_from_name(String class_name) {
    return primitive_classes_names.get(class_name);
  }

  /** Returns the class of an Array of the provided class with the provided
   *  number of dimensions.
   * Example : class_of_array_of(String.class, 2) returns the same class than
   *           String[][].class
   * an IllegalArgumentException will be thrown, if :
   * - the number of dimensions is inferior to 1;
   * - the class provided cannot be the component of an Array (e.g. : void)
   * @param component_class  the component class of the Array
   * @param dimensions  the number of dimensions in the Array
   * @return the class of an Array of the provided class, with the provided
   *         number of dimensions.
   * @throws IllegalArgumentException if dimensions is inferior to 1, or if
   *                                  the class cannot be the component of
   *                                  an Array.
   */
  public static Class class_of_array_of(Class component_class, 
                                        int dimensions) {
    return Array.newInstance(component_class, new int[dimensions]).getClass();
  }

  /** Given a literal representation of a class, this will return the
   *  represented class.
   * Note : No implicit import directives should be implied in the literal
   * representation of a non-primitive class.
   * Examples :
   * - 'int'    will return int.class
   * - 'byte[]' will return byte[].class 
   * - 'java.lang.String[]' will return String[].class
   * - 'String' will throw a ClassNotFoundException
   * - 'void[]' will throw an IllegalArgumentException
   * @param literal  The literal representation of the desired class in Java.
   *                 Examples : 'int', 'java.lang.String[][][]', 'float[]'.
   * @return the class represetend by the literal or null, if no literal equals
   *         null or is empty.
   * @throws ClassNotFoundException if no such class exist, according to the
   *                                JVM in use.
   * @throws IllegalArgumentException if the literal try to define an Array that
   *                                  cannot be produced by the JVM.
   */
  public static Class class_from_literal(String literal) 
    throws ClassNotFoundException {
    if (literal == null ||
        literal.isEmpty()) {
        return null;
    }
    
    /* Ugly way to count dimensions precised in the original literal.
       Remove every occurence of a dimension literal '[]' from the literal,
       compute the size difference and divide it by the size of '[]' to infer
       the number of array dimensions (defined by the removed literals) in the
       original literal.
       Example :
       Let 'java.lang.String[][]' be the original literal. Once we strip the
       dimension literals '[]', it becomes 'java.lang.String'. The length 
       between 'java.lang.String[][]' and java.lang.String' is four characters.
       Since the '[]' takes two characters, we divide the four characters
       difference by two (the length of '[]') and we get two, meaning that two 
       '[]' were precised in original literal, so the number of dimensions is 2.
       
       Note : This doesn't check where the brackets were precised.
       '[][]java.lang.String' or 'ja[]va.la[]ng.String' would also pass, but
       that's not a concern. If the resulting classname is invalid,
       ClassNotFoundException will be thrown, and if it's valid, then there is
       no problem to be concerned about... This is not a syntax validator.
    */
    final String literal_without_brackets = literal.replace("[]", "");
    final int array_dimensions = 
      (literal.length() - literal_without_brackets.length()) / 
      "[]".length();
    
    // primitive_from_name returns null if the string passed doesn't represent
    // a primitive literal name.
    Class main_class = primitive_from_name(literal_without_brackets);
    if (main_class == null) { 
      main_class = Class.forName(literal_without_brackets); 
    }
     
    if (array_dimensions == 0) { return main_class; }
    else { return class_of_array_of(main_class, array_dimensions); }
  }

  /** Returns the literal name of a class. 
   * Examples : 
   * - int[][].class  returns "int[][]".
   * - String.class   returns "java.lang.String"
   * - String[].class returns "java.lang.String[]"
   * @param object_class  The class to represent literally.
   * @return the literal representation of the provided class.
   * @throws NullPointerException if the provided argument is null
   */
  public static String class_to_literal(Class object_class) {
    int array_dimensions = 0;
    Class component_class = object_class;

    while (component_class.isArray()) {
      array_dimensions++;
      component_class = component_class.getComponentType();
    }

    StringBuilder class_name = new StringBuilder(component_class.getName());

    while (array_dimensions-- != 0) { class_name.append("[]"); }
    
    return class_name.toString();
  }

  /** Checks if the provided class is Float.class, Double.class or
   *  java.math.BigDecimal.class. 
   * The last class check is the main difference between 
   * <code>is_decimal_Number</code> and <code>is_floating_point</code>.
   * @param analysed_class  The class to check
   * @return true if the provided class is Float.class, Double.class or 
   *         java.math.BigDecimal.class, false otherwise. 
   */
  public static boolean is_decimal_Number(Class analysed_class) {
    return (analysed_class == Float.class ||
            analysed_class == Double.class ||
            analysed_class == java.math.BigDecimal.class);
  }

  /** Checks if the class of the provided object is Float.class, Double.class 
   *  or java.math.BigDecimal.class.
   * The last class check is the main difference between
   * <code>is_decimal_Number</code> and <code>is_floating_point</code>
   * @param analysed_object  The object to check
   * @return true if the class of the provided object is Float.class, 
   *         Double.class or java.math.BigDecimal.class, false otherwise.
   */
  public static boolean is_decimal_Number(Object analysed_object) {
    if (analysed_object == null) return false;
    return is_decimal_Number(analysed_object.getClass()); 
  }

  /** Checks if the provided class is the primitive type 'float' or the class 
   *  java.lang.Float.
   * @param analysed_class  The provided class to check
   * @return true if the class is float.class or Float.class; false otherwise.
   */
  public static boolean is_float(Class analysed_class) {
    return (analysed_class == float.class ||
            analysed_class == Float.class);
  }

  /** Checks if the provided object class is 'java.lang.Float'.
   * @param analysed_object  The provided object to check
   * @return true if the class of the provided object is java.lang.Float
   */
  public static boolean is_float(Object analysed_object) {
    return (analysed_object.getClass() == Float.class);
  }

  /** Special signature of is_float that always return true.
   * @param float_value  a float value
   * @return true
   */
  public static boolean is_float(float float_value) {
    return true;
  }
   
  /** Special signature of is_float that always return true.
   * @param Float_value  a Float value
   * @return true
   */
  public static boolean is_float(Float Float_value) {
    return true;
  }

  /** Special signature of is_float that always return false.
   * This special signature avoids integer-typed values to be converted to 
   * 'float' and passed to is_float(float) through automatic method conversion.
   * @param integer_value  an integer value
   * @return false
   */
  public static boolean is_float(long integer_value) {
    return false;
  }

  /** Special signature of is_float that always return false.
   * @param double_value  a double value
   * @return false
   */
  public static boolean is_float(double double_value) {
    return false;
  }

  /** Checks if the provided class is a primitive class (unboxed or boxed)
   *  representing floating-point numbers.
   * /!\ Does not check if the provided argument is null /!\
   * @param analysed_class  The provided class to check
   * @return true if the class is a primitive class representing floating-point
   *         numbers; false otherwise;
   */
  public static boolean is_floating_point(Class analysed_class) {
    boolean check_result = (analysed_class == float.class ||
                            analysed_class == Float.class ||
                            analysed_class == double.class ||
                            analysed_class == Double.class);
    return check_result;
  }

  /** Checks if the provided object is a direct instance of a primitive class
   *  representing floating-point numbers.
   * @param analysed_object  The object analysed.
   * @return true if the object is an instance of a primitive class representing
   *         floating-point numbers; false otherwise.
   */
  public static boolean is_floating_point(Object analysed_object) {
    if (analysed_object == null) return false;
    return is_floating_point(analysed_object.getClass());
  }

  /** Special signature of is_floating_point that will return true directly, 
   *  avoiding undue checks when passing a 'float'.
   *  Returns true automatically.
   * @param floating_point_value  A float value.
   * @return true
   */ 
  public static boolean is_floating_point(float floating_point_value) {
    return true;
  }

  /** Special signature of is_floating_point that will return true directly, 
   *  avoiding undue checks when passing a 'Float'.
   *  Returns true automatically.
   * @param floating_point_value  A Float value.
   * @return true
   */ 
  public static boolean is_floating_point(Float floating_point_value) {
    return true;
  }

  /** Special signature of is_floating_point that will return true directly,
   *  avoiding undue checks when passing a 'double'.
   *  Returns true automatically.
   * @param floating_point_value  A double value.
   * @return true
   */
  public static boolean is_floating_point(double floating_point_value) {
    return true;
  }

  /** Special signature of is_floating_point that will return true directly,
   *  avoiding undue checks when passing a 'Double'.
   *  Returns true automatically.
   * @param floating_point_value  A Double value.
   * @return true
   */
  public static boolean is_floating_point(Double floating_point_value) {
    return true;
  }

  /** Special signature of is_floating_point that will return false directly,
   *  avoiding undue checks when passing an integer value.
   *  Returns false automatically.
   * @param integer_value  an integer value.
   * @return false
   */
  public static boolean is_floating_point(long integer_value) {
    return false;
  }

  /** Checks if the specified Number represents a special floating point value.
   *  Examples :
   *  - NaN, Infinity or -Infinity values return true.
   * @param number  the number checked.
   * @return true if the specified number represents a special floating point
   *         value; false otherwise.
   */
  public static boolean is_special_floating_point(Number number) {
    return (special_floating_point_values.contains(number));
  }

  /** Checks if the class provided is boolean or Boolean.
   * @param analysed_class  the class to check
   * @return true if the class provided is boolean.class or Boolean.class
   */
  public static boolean is_boolean(Class analysed_class) {
    return (analysed_class == boolean.class ||
            analysed_class == Boolean.class);
  }

  /** Checks if the object provided is an instance of boolean or Boolean.
   * @param analysed_object  the object to check.
   * @return true if the object is an instance of boolean or Boolean.
   */
  public static boolean is_boolean(Object analysed_object) {
    if (analysed_object == null) return false;
    return is_boolean(analysed_object.getClass());  
  }

  /** Special signature of is_boolean that automatically return true.
   * @param boolean_value  a boolean value
   * @return true
   */
  public static boolean is_boolean(boolean boolean_value) {
    return true;
  }

  /** Special signature of is_boolean that atuomatically return true.
   * @param boolean_value  a Boolean value
   * @return true
   */
  public static boolean is_boolean(Boolean boolean_value) {
    return true;
  }

  /** Checks if the class provided is char or Character.
   * @param analysed_class  the class to check
   * @return true if the class is char or Character, false otherwise.
   */
  public static boolean is_char(Class analysed_class) {
    return (analysed_class == char.class ||
            analysed_class == Character.class);
  }

  /** Checks if the object provided is an instance of char or Character.
   * @param analysed_object  the object to check
   * @return true if the object is an instance of char or Character
   */
  public static boolean is_char(Object analysed_object) {
    if (analysed_object == null) { return false; }
    return is_char(analysed_object.getClass());
  }

  /** Special signature of is_char that always return true
   * @param char_value  a char value
   * @return true
   */
  public static boolean is_char(char char_value) {
    return true;
  }

  /** Special signature of is_char that always return true
   * @param character_value  a Character value
   * @return true
   */
  public static boolean is_char(Character character_value) {
    return true;
  }

  /** Checks if the class provided is void or Void.
   * @param analysed_class  the class to check
   * @return true if the class is void or Void
   */
  public static boolean is_void(Class analysed_class) {
    return (analysed_class == void.class ||
            analysed_class == Void.class);
  }

  /** Checks if the object is null.
   * @param analysed_object  the object to analyse
   * @return true if the object is null, false otherwise.
   */
  public static boolean is_null(Object analysed_object) {
    return (analysed_object == null);
  }

  /** Special signature of is_null that always return true
   * Void value = null; is perfectly valid and compiles...
   * @param void_value  a Void value
   * @return true
   */
  public static boolean is_null(Void void_value) {
    return true;
  }

  /**
   * Get the component class of the last dimension of the Array <code>o</code>.
   * It will throw java.lang.IllegalArgumentException if the argument
   * class does not respond to isArray().
   * </p>
   * Meaning that, for example :
   * - <code>int[] a</code> and <code>int[][][] a</code> will both return 
   *   int.class.
   * - <code>ArrayList</code> will return a 
   *   <code>java.lang.IllegalArgumentException</code> since 
   *   <code>ArrayList.isArray()</code> will return false) !
   * @param array  The Array Object to get the component class from.
   * @throws IllegalArgumentException  if <code>o.getClass().isArray()</code>
   * returns false or if <code>o == null</code>.
   * @return the component class of the last dimension of <code>o</code>
   */
  public static Class get_component_class_of(Object array) {

    if (array == null || !array.getClass().isArray()) {
      String class_name;
      if (array == null) { class_name = "null"; }
      else { class_name = array.getClass().getName(); }
      throw new IllegalArgumentException("Expected an array, got a :"+
                                         class_name);
    }

    Class component_type;
    for (component_type = array.getClass().getComponentType();
         component_type.isArray();
         component_type = component_type.getComponentType());
    
    return component_type;
  }


}
