package com.miouyouyou.libraries.helpers;

/** The interface used for objects casting any primitive number, boxed or 
 *  unboxed, to any boxed primitive value.
 * The boxed requirement of the output value is due to the return type of
 * the casting methods.
 */
public interface PrimitiveBoxedCaster {
  public Object cast(double value);
  public Object cast(long value);
}
