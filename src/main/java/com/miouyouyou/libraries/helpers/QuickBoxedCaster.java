package com.miouyouyou.libraries.helpers;

/* These classes are referenced implicitly, through reflection... */
import com.miouyouyou.libraries.helpers.casters.CastToByte;
import com.miouyouyou.libraries.helpers.casters.CastToChar;
import com.miouyouyou.libraries.helpers.casters.CastToShort;
import com.miouyouyou.libraries.helpers.casters.CastToInt;
import com.miouyouyou.libraries.helpers.casters.CastToLong;
import com.miouyouyou.libraries.helpers.casters.CastToFloat;
import com.miouyouyou.libraries.helpers.casters.CastToDouble;

import com.miouyouyou.libraries.helpers.AbstractPrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.JavaClassHelpers;
import static com.miouyouyou.libraries.helpers.JavaStringHelpers.capitalise;

/** QuickBoxedCaster provide instance method to cast any primitive number, 
 *  boxed or not, to a boxed primitive number. The class of the casted number
 *  is defined during the construction of the object.
 * The methods provide ability to accept or reject overflowing numbers, and
 * provide a little control over the overflowing method, if overflowing is
 * desired.
 * <pre>
 * {@code
 * Example of use :
 * import com.miouyouyou.libraries.helpers.AbstractPrimitiveBoxedCaster;
 * import com.miouyouyou.libraries.helpers.QuickBoxedCaster;
 * 
 * public class Test {
 *   public static void main(String[] args) {
 *     AbstractPrimitiveBoxedCaster caster = new QuickBoxedCaster(short.class);
 *     Double dvalue  = 11278.7D;
 *     long lvalue    = -8714L;
 *     Integer ivalue = 32000;
 *     
 *     print_val(caster.cast(dvalue)); // 11278
 *     print_val(caster.cast(lvalue)); // -8714
 *     print_val(caster.cast(ivalue)); // 32000
 *   }
 * 
 *   public static void print_value(short value) {
 *     System.out.println(value);
 *   }
 * }
 * }
 * </pre>
 */
public class QuickBoxedCaster extends AbstractPrimitiveBoxedCaster {

  private PrimitiveBoxedCaster caster;

  public static IllegalArgumentException null_not_allowed_exception =
    new IllegalArgumentException("Expected a class, got null instead");
  public static IllegalArgumentException boolean_not_allowed_exception =
    new IllegalArgumentException("Cannot cast to boolean...\n"+
                                 "Don't be fooled by the JLS §4.2.5\n"+
                                 "Only '==' and '!=' operators convert "+
                                 "objects and primitives to boolean...");
  public static IllegalArgumentException primitive_required_exception =
    new IllegalArgumentException("This class requires a primitive or boxed "+
                                 "primitive class as argument...");

  /** This constructs a caster that will cast any primitive number, 
   *  boxed or not, to a boxed primitive number.
   * This constructor provides no control of the overflowing behaviour.
   * Meaning that if an overflow occurs during a cast, the overflowing behaviour
   * is defined by the JVM used.
   * @param primitive_class  the boxed primitive class of the casted numbers.
   *                         Unboxed primitive classes will still result in boxed
   *                         primitive numbers (int.class -> Integer numbers)
   * @throws IllegalArgumentException  If a null reference is provided instead of
   *                                   a class; the class provided as argument do
   *                                   not represent primitive numbers
   *                                   (e.g : char.class and Character.class won't
   *                                   trigger the exception, boolean.class will).
   */
  public QuickBoxedCaster(Class primitive_class) {
    this(primitive_class, false, false);
  }

  /** This constructs a caster that will cast any primitive number,
   *  boxed or not, to a boxed primitive number. Overflowing values might be
   *  set to the nearest limit of the casted type, or be casted normally.
   * If the second value is set to true, overflowing numbers during a cast will
   * be set to the nearest number representable by the casted type.
   * E.g. : QuickBoxedCaster(byte.class, true).cast( 130); ->  127
   * E.g. : QuickBoxedCaster(byte.class, true).cast(-130); -> -128
   * If set to false, the overflowing behaviour is defined by the JVM.
   * @param primitive_class  the boxed primitive class of the casted numbers.
   *                         Unboxed primitive classes will still result in boxed
   *                         primitive numbers (int.class -> Integer numbers)
   * @param bound_to_minmax  determines if overflowing values are to be set to
   *                         nearest number representable by the provided class.
   * @throws IllegalArgumentException  If a null reference is provided instead of
   *                                   a class; the class provided as argument do
   *                                   not represent primitive numbers
   *                                   (e.g : char.class and Character.class won't
   *                                   trigger the exception, boolean.class will).
   */
  public QuickBoxedCaster(Class primitive_class, boolean bound_to_minmax) {
    this(primitive_class, bound_to_minmax, false);
  }

  /** This constructs a caster that will cast any primitive number,
   *  boxed or not, to a boxed primitive number. Overflowing numbers might : be
   *  set to the nearest limit of the casted type; trigger a
   *  NumberFormatException; or be casted normally.
   * If the second value is set to true, overflowing numbers during a cast will
   * be set to the nearest number representable by the casted type.
   * If the third value is set to true, overflowing numbers will trigger a
   * NumberFormatException.
   * @param primitive_class  the boxed primitive class of the casted numbers.
   *                         Unboxed primitive classes will still result in boxed
   *                         primitive numbers (int.class -> Integer numbers)
   * @param bound_to_minmax  determines if overflowing numbers are to be set to
   *                         nearest number representable by the provided class.
   * @param throw_exception_on_overflow  determines if overflowing numbers trigger
   *                                     NumberFormatException.
   * @throws IllegalArgumentException  If a null reference is provided instead of
   *                                   a class; the class provided as argument do
   *                                   not represent primitive numbers
   *                                   (e.g : char.class and Character.class won't
   *                                   trigger the exception, boolean.class will).
   */
  public QuickBoxedCaster(Class primitive_class, boolean bound_to_minmax,
                          boolean throw_exception_on_overflow) {

    final Class caster_type;
    final Class<PrimitiveBoxedCaster> caster_class;
    /* void IS A PRIMITIVE TYPE ! Don't believe what's written on the net, do
       a simple fact check !
       System.out.println(void.class.isPrimitive()); -> true
    */
    if (primitive_class == null ||
        primitive_class == void.class) { throw null_not_allowed_exception; }
    if (!primitive_class.isPrimitive()) { 
      caster_type = 
        JavaClassHelpers.get_primitive_equivalent_of(primitive_class);
      if (caster_type == null) { 
        throw primitive_required_exception; }
    } else { 
      caster_type = primitive_class; 
    }
    if (primitive_class == boolean.class) 
      { throw boolean_not_allowed_exception; }

    final String caster_classname_suffix;
    if (!bound_to_minmax && !throw_exception_on_overflow) {
      caster_classname_suffix = "Standard";
    } else if (bound_to_minmax && !throw_exception_on_overflow) {
      caster_classname_suffix = "BoundToMinMax"; 
    } else { // (throw_exception_on_overflow)
      caster_classname_suffix = "ThrowOnOverflow";
    }

    final String caster_classname = 
      caster_class_name(caster_type, caster_classname_suffix);
    try {
      this.caster  = (PrimitiveBoxedCaster) Class.forName(caster_classname).newInstance();
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    } catch (InstantiationException e) { 
      throw new RuntimeException(e); 
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } // catch (ExceptionInInitializerError e) { throw new RuntimeException(e); }
  }

  /** This helper method compose the class name of the caster that will be used.
   * @param caster_type_class  a primitive class. (int.class, char.class, ...)
   * @param caster_classname_suffix  Since every caster is decomposed into 3
   *                                 subclasses, this defines which subclass
   *                                 should be inserted in the name.
   * @return The name of the caster class to instantiate.
   */
  private static String caster_class_name(Class caster_type_class, 
                                          String caster_classname_suffix) {
    final StringBuilder full_class_name = new StringBuilder();
    full_class_name.append("com.miouyouyou.libraries.helpers.casters.");
    full_class_name.append("CastTo");
    full_class_name.append(capitalise(caster_type_class.getName()));
    /* When you use reflection, the static subclass MainClass.SubClass becomes
       MainClass$SubClass
       Go figure... */
    full_class_name.append("$");
    full_class_name.append(caster_classname_suffix);
    return full_class_name.toString();    
  }

  /** Cast the provided value to a number, which class is defined during this
   *  object's construction.
   * The reason why Object was used, instead of Number, is because 'char' and 
   * 'Character' are primitive types that can represent unsigned 16-bit numbers,
   * but 'Character', the boxed version of 'char', does not implement the 
   * 'Number' interface...
   * @param value  A floating-point value.
   * @return a number, wrapped into an Object, which class is defined during
   *         this object's construction.
   * @throws NumberFormatException  if, during the construction of this object,
   *                                throwing an exception when encountering an
   *                                overflowing value was desired.
   */
  public Object cast(double value) {
    return this.caster.cast(value);
  }

  /** Cast the provided value to a number, which class is defined during this
   *  object's construction.
   * The reason why Object was used, instead of Number, is because 'char' and 
   * 'Character' are primitive types that can represent unsigned 16-bit numbers,
   * but 'Character', the boxed version of 'char', does not implement the 
   * 'Number' interface...
   * @param value  An integer value.
   * @return a number, wrapped into an Object, which class is defined during
   *         this object's construction.
   * @throws NumberFormatException  if, during the construction of this object,
   *                                throwing an exception when encountering an
   *                                overflowing value was desired.
   */  
  public Object cast(long value) {
    return this.caster.cast(value);
  }
}
