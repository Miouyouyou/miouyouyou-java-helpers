package com.miouyouyou.libraries.helpers;

/** This class provide multiple static methods to :
 *  - cast any primitive, boxed or unboxed, to any unboxed primitive type.
 *    These cast methods will either :
 *   * perform a simple (primitive) cast;
 *   * restrict the casted value to the bounds of the new primitive type,
 *     providing a determined behaviour on overflow;
 *   * throw a NumberFormatException an exception if the cast would provoke an
 *     overflow.
 *  - limit a value to a specified interval.
 *  - check if a value fit in an interval and throw a NumberFormatException, 
 *    if not.
 */

public class JavaCastHelpers {
  private static final long 
    BYTE_MAX_POSITIVE_VALUE      = (long) Byte.MAX_VALUE,
    BYTE_MAX_NEGATIVE_VALUE      = (long) Byte.MIN_VALUE,
    SHORT_MAX_POSITIVE_VALUE     = (long) Short.MAX_VALUE,
    SHORT_MAX_NEGATIVE_VALUE     = (long) Short.MIN_VALUE,
    CHARACTER_MAX_POSITIVE_VALUE = (long) Character.MAX_VALUE,
    CHARACTER_MAX_NEGATIVE_VALUE = (long) Character.MIN_VALUE,
    INTEGER_MAX_POSITIVE_VALUE   = (long) Integer.MAX_VALUE,
    INTEGER_MAX_NEGATIVE_VALUE   = (long) Integer.MIN_VALUE,
    LONG_MAX_POSITIVE_VALUE      = Long.MAX_VALUE,
    LONG_MAX_NEGATIVE_VALUE      = Long.MIN_VALUE;

  private static final double 
    FLOAT_MAX_POSITIVE_VALUE     = (double) Float.MAX_VALUE,
    FLOAT_MAX_NEGATIVE_VALUE     = (double) -Float.MAX_VALUE,
    DOUBLE_MAX_POSITIVE_VALUE    = Double.MAX_VALUE,
    DOUBLE_MAX_NEGATIVE_VALUE    = -Double.MAX_VALUE;

  /** Returns a value, or the nearest interval limit if the value doesn't
   *  fit in the defined inclusive interval.
   * If the value is not a number or fails every interval check, the 
   * value is returned as is. E.g. if you pass a NaN, you'll get a NaN.
   * @param value  The value tested against the interval
   * @param min  The minimal value of the inclusive interval
   * @param max  The maximal value of the inclusive interval
   * @return the value, bounded to the limits of the defined interval
   */
  public static double value_or_minmax(double value,
                                       double min,
                                       double max) {
    if (value >= min && value <= max) { return value; }
    else if (value < min) { return min; }
    else if (value > max) { return max; }
    else { return value; }
  }
  /** Returns a value, or the nearest interval limit if the value doesn't
   *  fit in the defined inclusive interval.
   * If the value is not a number or fails every interval check, the 
   * value is returned as is. E.g. if you pass a NaN, you'll get a NaN.
   * @param value  The value tested against the interval
   * @param min  The minimal value of the inclusive interval
   * @param max  The maximal value of the inclusive interval
   * @return the value, bounded to the limits of the defined interval
   */
  public static long value_or_minmax(long value,
                                     long min,
                                     long max) {
    if (value >= min && value <= max) { return value; }
    else if (value < min) { return min; }
    else if (value > max) { return max; }
    else { return value; }
  }
  // Exception throwed if the value exceeds defined bounds 
  // in the bound_check method
  public static final NumberFormatException value_out_of_bounds = 
    new NumberFormatException("Value out of defined bounds");
  /** Returns the provided value or a NumberFormatException, if the value
   *  exceed the limits of the inclusive interval defined.
   * @param value  The value tested against the interval
   * @param min  The minimal value of the inclusive interval
   * @param max  The maximal value of the inclusive interval
   * @return the value
   * @throws NumberFormatException if the value doesn't fit in the interval
   */
  public static double bound_check(double value,
                                   double min,
                                   double max) {
    if (value >= min && value <= max) { return value; }
    throw value_out_of_bounds;
  }
  /** Returns the provided value or a NumberFormatException, if the value
   *  exceed the limits of the inclusive interval defined.
   * @param value  The value tested against the interval
   * @param min  The minimal value of the inclusive interval
   * @param max  The maximal value of the inclusive interval
   * @return the value
   * @throws NumberFormatException if the value doesn't fit in the interval
   */
  public static long bound_check(long value,
                                 long min,
                                 long max) {
    if (value >= min && value <= max) { return value; }
    throw value_out_of_bounds;
  }
  /** Cast the value to double. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  Infinite for positive overflows
   * -> -Infinite for negative overflows
   * @param value  the value to cast.              
   * @return the value casted to double, with appropriate changes if 
   *         an overflow occured.
   */
  public static double to_double(double value) {
    return value;
  }

  /** Cast the provided value to double. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Double.MAX_VALUE for positive values
   *  - -Double.MAX_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a double, and bounded
   *          to the limits of this type.
   */
  public static double to_double_bounded_to_minmax(double value) {
    return (double) value_or_minmax(value,
                                    DOUBLE_MAX_NEGATIVE_VALUE,
                                    DOUBLE_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to double. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * Note that denormalisation can still happen !
   * @param value  The value to cast
   * @return  The value casted to double, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static double to_double_throw_on_overflow(double value) {
    return (double) bound_check(value,
                                DOUBLE_MAX_NEGATIVE_VALUE,
                                DOUBLE_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to float. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  Infinite for positive overflows
   * -> -Infinite for negative overflows
   * @param value  the value to cast.              
   * @return the value casted to float, with appropriate changes if 
   *         an overflow occured.
   */
  public static float to_float(double value) {
    return (float) value;
  }

  /** Cast the provided value to float. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Float.MAX_VALUE for positive values
   *  - -Float.MAX_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a float, and bounded
   *           to the limits of this type.
   */
  public static float to_float_bounded_to_minmax(double value) {
    return (float) value_or_minmax(value,
                                   FLOAT_MAX_NEGATIVE_VALUE,
                                   FLOAT_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to float. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * Note that denormalisation can still happen !
   * @param value  The value to cast
   * @return  The value casted to float, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static float to_float_throw_on_overflow(double value) {
    return (float) bound_check(value,
                               FLOAT_MAX_NEGATIVE_VALUE,
                               FLOAT_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to long. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  9223372036854775807 for positive overflows
   * -> -9223372036854775808 for negative overflows.
   * @param value  the value to cast.              
   * @return the value casted to long, with appropriate changes if 
   *         an overflow occured.
   */
  public static long to_long(double value) {
    return (long) value;
  }

  /** Cast the provided value to long. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Long.MAX_VALUE for positive values
   *  - Long.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a long, and bounded
   *           to the limits of this type.
   */
  public static long to_long_bounded_to_minmax(double value) {
    return (long) value_or_minmax(value,
                                  LONG_MAX_NEGATIVE_VALUE,
                                  LONG_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to long. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to long, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static long to_long_throw_on_overflow(double value) {
    return (long) bound_check(value,
                              LONG_MAX_NEGATIVE_VALUE,
                              LONG_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to long. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ((input_value + 2^63) % 2^64) - 2^63
   * @param value  the value to cast.              
   * @return the value casted to long, with appropriate changes if 
   *         an overflow occured.
   */
  public static long to_long(long value) {
    return value;
  }

  /** Cast the provided value to long. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   * This just return the value provided as is, since the value
   * type and the cast type are the same AND they are integers.
   * If the value were to overflow, it would already have over-
   * flowed before reaching this method, and integers do not
   * produce special values when an overflow occur (like Infinity),
   * so there's no way to know it overflowed before reaching the
   * method and take appropriate measures.
   * @param value  a long value.
   * @return the long value
   */
  public static long to_long_bounded_to_minmax (long value) {
    return value;
  }

  /** Cast the provided value to long. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * This just return the value provided as is, since the value
   * type and the cast type are the same AND they are integers.
   * If the value were to overflow, it would already have over-
   * flowed before reaching this method, and integers do not
   * produce special values when an overflow occur (like Infinity),
   * so there's no way to know it overflowed before reaching the
   * method and take appropriate measures.
   * @param value  a long value.
   * @return the long value
   */
  public static long to_long_throw_on_overflow (long value) {
    return value;
  }

  /** Cast the value to int. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  2147483647 for positive overflows
   * -> -2147483648 for negative overflows.
   * @param value  the value to cast.              
   * @return the value casted to int, with appropriate changes if 
   *         an overflow occured.
   */
  public static int to_int(double value) {
    return (int) value;
  }

  /** Cast the provided value to int. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Integer.MAX_VALUE for positive values
   *  - Integer.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to an int, and bounded
   *           to the limits of this type.
   */
  public static int to_int_bounded_to_minmax(double value) {
    return (int) value_or_minmax(value,
                                 INTEGER_MAX_NEGATIVE_VALUE,
                                 INTEGER_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to int. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to int, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static int to_int_throw_on_overflow(double value) {
    return (int) bound_check(value,
                             INTEGER_MAX_NEGATIVE_VALUE,
                             INTEGER_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to int. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ((input_value + 2^31) % 2^32) - 2^31
   * @param value  the value to cast.              
   * @return the value casted to int, with appropriate changes if 
   *         an overflow occured.
   */
  public static int to_int(long value) {
    return (int) value;
  }

  /** Cast the provided value to int. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Integer.MAX_VALUE for positive values
   *  - Integer.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to an int, and bounded
   *           to the limits of this type.
   */
  public static int to_int_bounded_to_minmax(long value) {
    return (int) value_or_minmax(value,
                                 INTEGER_MAX_NEGATIVE_VALUE,
                                 INTEGER_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to int. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to int, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static int to_int_throw_on_overflow(long value) {
    return (int) bound_check(value,
                             INTEGER_MAX_NEGATIVE_VALUE,
                             INTEGER_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to char. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  65535 for positive overflows
   * -> 0 for negative overflows.
   * @param value  the value to cast.              
   * @return the value casted to char, with appropriate changes if 
   *         an overflow occured.
   */
  public static char to_char(double value) {
    return (char) value;
  }

  /** Cast the provided value to char. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Character.MAX_VALUE for positive values
   *  - Character.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a char, and bounded
   *           to the limits of this type.
   */
  public static char to_char_bounded_to_minmax(double value) {
    return (char) value_or_minmax(value,
                                  CHARACTER_MAX_NEGATIVE_VALUE,
                                  CHARACTER_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to char. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to char, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static char to_char_throw_on_overflow(double value) {
    return (char) bound_check(value,
                              CHARACTER_MAX_NEGATIVE_VALUE,
                              CHARACTER_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to char. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * overflowing_value % 2^16
   * @param value  the value to cast.              
   * @return the value casted to char, with appropriate changes if 
   *         an overflow occured.
   */
  public static char to_char(long value) {
    return (char) value;
  }

  /** Cast the provided value to char. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Character.MAX_VALUE for positive values
   *  - Character.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a char, and bounded
   *           to the limits of this type.
   */
  public static char to_char_bounded_to_minmax(long value) {
    return (char) value_or_minmax(value,
                                  CHARACTER_MAX_NEGATIVE_VALUE,
                                  CHARACTER_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to char. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to char, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static char to_char_throw_on_overflow(long value) {
    return (char) bound_check(value,
                              CHARACTER_MAX_NEGATIVE_VALUE,
                              CHARACTER_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to short. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  32767 for positive overflows
   * -> -32768 for negative overflows.
   * @param value  the value to cast.              
   * @return the value casted to short, with appropriate changes if 
   *         an overflow occured.
   */
  public static short to_short(double value) {
    return (short) value;
  }

  /** Cast the provided value to short. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Short.MAX_VALUE for positive values
   *  - Short.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a short, and bounded
   *           to the limits of this type.
   */
  public static short to_short_bounded_to_minmax(double value) {
    return (short) value_or_minmax(value,
                                   SHORT_MAX_NEGATIVE_VALUE,
                                   SHORT_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to short. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to short, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static short to_short_throw_on_overflow(double value) {
    return (short) bound_check(value,
                               SHORT_MAX_NEGATIVE_VALUE,
                               SHORT_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to short. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ((input_value + 2^15) % 2^16) - 2^15
   * @param value  the value to cast.              
   * @return the value casted to short, with appropriate changes if 
   *         an overflow occured.
   */
  public static short to_short(long value) {
    return (short) value;
  }

  /** Cast the provided value to short. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Short.MAX_VALUE for positive values
   *  - Short.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a short, and bounded
   *           to the limits of this type.
   */
  public static short to_short_bounded_to_minmax(long value) {
    return (short) value_or_minmax(value,
                                   SHORT_MAX_NEGATIVE_VALUE,
                                   SHORT_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to short. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to short, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static short to_short_throw_on_overflow(long value) {
    return (short) bound_check(value,
                               SHORT_MAX_NEGATIVE_VALUE,
                               SHORT_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to byte. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ->  127 for positive overflows
   * -> -128 for negative overflows.
   * @param value  the value to cast.              
   * @return the value casted to byte, with appropriate changes if 
   *         an overflow occured.
   */
  public static byte to_byte(double value) {
    return (byte) value;
  }

  /** Cast the provided value to byte. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Byte.MAX_VALUE for positive values
   *  - Byte.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a byte, and bounded
   *           to the limits of this type.
   */
  public static byte to_byte_bounded_to_minmax(double value) {
    return (byte) value_or_minmax(value,
                                  BYTE_MAX_NEGATIVE_VALUE,
                                  BYTE_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to byte. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to byte, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static byte to_byte_throw_on_overflow(double value) {
    return (byte) bound_check(value,
                              BYTE_MAX_NEGATIVE_VALUE,
                              BYTE_MAX_POSITIVE_VALUE);
  }

  /** Cast the value to byte. If an overflow occurs, the current 
   *  Java implementation will dictate what value will be returned.
   * Generally, when using this method with a Java implementation mimicking 
   * the Sun/Oracle Java implementation, if an overflow occurs, expect the 
   * value to be equal to :
   * ((input_value + 2^7) % 2^8) - 2^7
   * @param value  the value to cast.              
   * @return the value casted to byte, with appropriate changes if 
   *         an overflow occured.
   */
  public static byte to_byte(long value) {
    return (byte) value;
  }

  /** Cast the provided value to byte. If the value exceed the limits
   *  of this type, the nearest limit value will be returned.
   *  That is :
   *  - Byte.MAX_VALUE for positive values
   *  - Byte.MIN_VALUE for negative values
   * @param value  the value to cast
   * @return  the value, casted to a byte, and bounded
   *           to the limits of this type.
   */
  public static byte to_byte_bounded_to_minmax(long value) {
    return (byte) value_or_minmax(value,
                                  BYTE_MAX_NEGATIVE_VALUE,
                                  BYTE_MAX_POSITIVE_VALUE);
  }

  /** Cast the provided value to byte. Throws a NumberFormatException if
   *  the cast provoke an overflow.
   * @param value  The value to cast
   * @return  The value casted to byte, guaranteed to not have overflowed
   * @throws NumberFormatException if the value were to overflow during the cast.
   */
  public static byte to_byte_throw_on_overflow(long value) {
    return (byte) bound_check(value,
                              BYTE_MAX_NEGATIVE_VALUE,
                              BYTE_MAX_POSITIVE_VALUE);
  }

}
