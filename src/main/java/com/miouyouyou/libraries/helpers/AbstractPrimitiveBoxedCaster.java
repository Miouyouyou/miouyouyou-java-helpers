package com.miouyouyou.libraries.helpers;

import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;

/** The only purpose of this class is to alleviate the impossibility
 *  to add 'default' methods to interfaces with Java 6, which cause
 *  problem with Android, since it uses Java 6...
 *  So this abstract class is used for glue code, when adding new
 *  methods to the interface 
 */
public abstract class AbstractPrimitiveBoxedCaster 
  implements PrimitiveBoxedCaster {
  
}
