package tests.com.miouyouyou.libraries.helpers;

import static com.miouyouyou.libraries.helpers.JavaExceptionHelpers.get_stacktrace_from;

import static junit.framework.Assert.assertTrue;
import junit.framework.TestCase;

public class JavaExceptionHelpersTest extends TestCase {
  public void testGetStacktraceFrom() {
    /* This test is difficult. The stacktrace printed by the JVM depends of
       the current implementation, as stated in Throwable#printStackTrace :
       "The format of this information depends on the implementation".

       So, during this test, false stacktraces are produced and set to
       throwables, through Throwable#setStackTrace(StackTraceElement[]); then 
       the stacktrace is acquired, as a String, with 
       JavaExceptionHelpers.get_stracktrace_from and the following informations
       are searched in the produced String :
       - the fully qualified name of the implicated throwables;
       - the informations precised in the false stacktraces (line numbers and
         class, methods and files names).

       If this test were to fail, be sure to check how the current 
       implementation running this test prints a valid stacktrace !
    */

    String first_message = "Something bad happened !";
    Throwable throwable = new Throwable(first_message);
    StackTraceElement[] throwable_stacktrace = new StackTraceElement[]
      {new StackTraceElement("farm.barn", "BadMooh", "Barn.java", 53)};
    throwable.setStackTrace(throwable_stacktrace);
    String acquired_throwable_stacktrace = get_stacktrace_from(throwable);

    assertTrue(acquired_throwable_stacktrace.contains(first_message));
    assertTrue(acquired_throwable_stacktrace.contains("java.lang.Throwable"));
    assertTrue(acquired_throwable_stacktrace.contains("farm.barn"));
    assertTrue(acquired_throwable_stacktrace.contains("BadMooh"));
    assertTrue(acquired_throwable_stacktrace.contains("53"));

    String second_message = "Dead by food poisoning";
    Exception exception = new Exception(second_message, throwable);
    StackTraceElement[] exception_stacktrace = new StackTraceElement[]
      {new StackTraceElement("hero.barbarian", "AteABadMooh", 
                             "Barbarian.java", 1474)};
    exception.setStackTrace(exception_stacktrace);
    String acquired_exception_stacktrace = get_stacktrace_from(exception);
    
    assertTrue(acquired_exception_stacktrace.contains(first_message));
    assertTrue(acquired_exception_stacktrace.contains(second_message));
    assertTrue(acquired_exception_stacktrace.contains("java.lang.Exception"));
    assertTrue(acquired_exception_stacktrace.contains("java.lang.Throwable"));
    assertTrue(acquired_exception_stacktrace.contains("hero.barbarian"));
    assertTrue(acquired_exception_stacktrace.contains("farm.barn"));
    assertTrue(acquired_exception_stacktrace.contains("1474"));
    assertTrue(acquired_exception_stacktrace.contains("53"));
  }
}
