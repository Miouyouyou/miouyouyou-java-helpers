package tests.com.miouyouyou.libraries.helpers;

import com.miouyouyou.libraries.helpers.JavaClassHelpers;

import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_from_literal;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_of_array_of;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_component_class_of;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_decimal_Number;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_primitive;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.primitive_from_name;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_primitive_equivalent_of;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_boxed_equivalent_of;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class JavaClassHelpersTest extends TestCase {
  public void testDecimalCheck() {
    Class[] decimal_Number_classes = new Class[]
      {Double.class, Float.class, BigDecimal.class};
    Class[] non_decimal_Number_classes = new Class[]
      {int.class, double.class, float.class, String.class, BigInteger.class, null};
    Object[] decimal_Numbers = new Object[] 
      {4.7, 8.7894, new BigDecimal("45.89E5")};
    Object[] non_decimal_objects = new Object[] 
      {"45.89E5", 4, 8, 7/2, new BigInteger("78"), null };

    for (Class clazz : decimal_Number_classes) {
      assertTrue(clazz.getName(), is_decimal_Number(clazz));
    }
    for (Object number : decimal_Numbers) {
      assertTrue(is_decimal_Number(number));
    }
    for (Class clazz : non_decimal_Number_classes) {
      assertFalse(is_decimal_Number(clazz));
    }
    for (Object object : non_decimal_objects) {
      assertFalse(is_decimal_Number(object));
    }
    
  }

  public void testCheckComponentClass() {
    Object[] int_arrays = new Object[] {
      new int[1][1][1][1], Array.newInstance(int.class, 1, 1, 1, 1)};
    Object[] object_arrays = {int_arrays, new Object[7]}; 
    Object[] non_arrays = new Object[] { new ArrayList<int[]>(), null };
    Object arraylist_array = new ArrayList[2];

    for (Object int_array : int_arrays) {
      assertEquals(int.class, 
                   get_component_class_of(int_array));
    }
    for (Object obj_array : object_arrays) {
      assertEquals(Object.class, 
                   get_component_class_of(obj_array));
    }
    boolean catched_the_exception = false;
    for (Object non_array : non_arrays) {
      
      try { get_component_class_of(non_array); } 
      catch (IllegalArgumentException e) { catched_the_exception = true; }
      assertTrue(catched_the_exception);
    }
    assertEquals(ArrayList.class, 
                 get_component_class_of(arraylist_array));
  }
  
  public void testNameToPrimitive() {
    Object[][] names_and_classes = 
      new Object[][] {{"byte",   byte.class}, 
                      {"short",  short.class},
                      {"char",   char.class},
                      {"int",    int.class},
                      {"long",   long.class},
                      {"float",  float.class},
                      {"double", double.class}};
    for (Object[] name_and_class : names_and_classes) {
      assertEquals((Class) name_and_class[1],
                   primitive_from_name((String) name_and_class[0]));
    }
  }

  public void testClassFromLiteral() throws ClassNotFoundException {
    Class int_array_class    = class_from_literal("int[]");
    Class string_array_class = class_from_literal("java.lang.String[]");
    Class string_class       = class_from_literal("java.lang.String");
    assertEquals(int[].class,    int_array_class);
    assertEquals(String[].class, string_array_class);
    assertEquals(String.class,   string_class);
  }

  public void testClassOfArrayOf() {
    assertEquals(int[].class, class_of_array_of(int.class, 1));
    assertEquals(String[][].class, class_of_array_of(String.class, 2));
    boolean exception_catched = false;
    try {
      class_of_array_of(void.class, 1);
    } catch (IllegalArgumentException e) { exception_catched = true; }
    assertTrue(exception_catched);
    exception_catched = false;
    try {
      class_of_array_of(String.class, 0);
    } catch (IllegalArgumentException e) { exception_catched = true; }
    assertTrue(exception_catched);
  }

  public void testisPrimitive() {
    Class[] primitive_classes = new Class[]
      {void.class, boolean.class, byte.class, short.class, char.class,
       int.class, long.class, float.class, double.class, Void.class,
       Boolean.class, Byte.class, Short.class, Character.class, Integer.class,
       Long.class, Float.class, Double.class};

    Class[] non_primitive_classes = new Class[]
      {null, Number.class, BigInteger.class, String.class};
    for (Class primitive_class : primitive_classes) {
      assertTrue(is_primitive(primitive_class));
    }
    for (Class non_primitive_class : non_primitive_classes) {
      assertFalse(is_primitive(non_primitive_class));
    }
  }

  public void testEquivalent() {
    Class[][] primitive_and_boxed_classes = new Class[][]
      {{void.class,    Void.class},
       {boolean.class, Boolean.class},
       {byte.class,    Byte.class},
       {short.class,   Short.class},
       {char.class,    Character.class},
       {int.class,     Integer.class},
       {long.class,    Long.class},
       {float.class,   Float.class},
       {double.class,  Double.class}};
    for (Class[] primitive_and_boxed_class : primitive_and_boxed_classes) {
      assertEquals(primitive_and_boxed_class[1],
                   get_boxed_equivalent_of(primitive_and_boxed_class[0]));
      assertEquals(primitive_and_boxed_class[0],
                   get_primitive_equivalent_of(primitive_and_boxed_class[1]));
    }
  }
}
