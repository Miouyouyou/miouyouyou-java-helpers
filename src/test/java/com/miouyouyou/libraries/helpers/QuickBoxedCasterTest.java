package tests.com.miouyouyou.libraries.helpers;

import com.miouyouyou.libraries.helpers.QuickBoxedCaster;

import java.lang.reflect.Method;

import junit.framework.Assert;
import junit.framework.TestCase;

public class QuickBoxedCasterTest extends TestCase {

  private Class[] primitive_classes;
  private Class[] boxed_classes;
  private Class[] denied_classes;

  private double[][] max_values; 
  private double[] overflow_values;

  private Number[] numbers;

  private Method check_method;

  public void setUp() throws Exception {
    super.setUp();
    this.primitive_classes = new Class[]
      {byte.class, char.class, short.class, int.class, long.class,
       float.class, double.class};
    this.boxed_classes = new Class[] 
      {Byte.class, Character.class, Short.class, Integer.class, Long.class,
       Float.class, Double.class};
    this.denied_classes = new Class[]
      {null, void.class, boolean.class, Number.class, Object.class};

  /* -Float.MAX_VALUE and -Double.MAX_VALUE are not errors...
     Look at the documentation of these classes for more information */

    this.max_values = new double[][] 
      {{(double) Byte.MIN_VALUE, (double) Byte.MAX_VALUE},
       {0D, (double) Character.MAX_VALUE},
       {(double) Short.MIN_VALUE, (double) Short.MAX_VALUE},
       {(double) Integer.MIN_VALUE, (double) Integer.MAX_VALUE},
       {(double) Long.MIN_VALUE, (double) Long.MAX_VALUE},
       {(double) -Float.MAX_VALUE, (double) Float.MAX_VALUE},
       {-Double.MAX_VALUE, Double.MAX_VALUE}};
    this.overflow_values = new double[] {
      Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY};


    this.numbers = new Number[] 
      {(Double) 2443289D, (Integer) 789545, (Byte) ((byte) 78)};


    this.check_method  = 
      QuickBoxedCasterTest.class.getDeclaredMethod("check_class_and_value",
                                                   Class.class,
                                                   Class.class,
                                                   double.class,
                                                   double.class);
  }

  public void testSimpleCast() {

    for (int index = 0; index < this.primitive_classes.length; index++) {
      Class primitive_class = this.primitive_classes[index];
      Class boxed_class     = this.boxed_classes[index];

      simpleCastTestMethodology(primitive_class, boxed_class);
      simpleCastTestMethodology(boxed_class, boxed_class);
    }
  }

  private void simpleCastTestMethodology(Class input_class, 
                                         Class expected_output_class) {
    long long_value = 37L;
    char char_value = (char) 31;
    Character character_value = (Character) char_value;
    byte byte_value = (byte) 59;

    Object a = new com.miouyouyou.libraries.helpers.casters.CastToByte.Standard();
    QuickBoxedCaster primitive_caster = new QuickBoxedCaster(input_class);
    QuickBoxedCaster boxed_caster     = new QuickBoxedCaster(input_class);
    QuickBoxedCaster no_overflow_caster = 
      new QuickBoxedCaster(input_class, true);
    QuickBoxedCaster except_on_overflow_caster = 
      new QuickBoxedCaster(input_class, true, true); 

    QuickBoxedCaster[] casters = new QuickBoxedCaster[]
      {primitive_caster, boxed_caster, no_overflow_caster,
       except_on_overflow_caster};
      
    Object output_value;

    /* So much things going on there... Let's answer some questions :
       - Why not factorize the output_value/check_class thing ?
       + I need to be sure that the caster accept any kind of primitive or
         boxed primitive value (byte, long, Character, ...)
         You might think that there might a way to factorize this, while 
         keeping the intent by iterating a raw ArrayList where the values are 
         stored ! But, too bad ! Doing this would automatically cast primitive 
         values to (Object) boxed versions, breaking the intent of the test !
         Creating one method per primitive and boxed type tested could work but
         given the one shot use, I would not call this factorizing...
       - Why calling check_class_and_value through reflection !?
       + Because the Java compiler cannot understand that QuickBoxedCaster#cast
         will return a boxed primitive number [1] that can be passed to the 
         method without problem, so it stops the compilation. The only way to 
         get through  'Java compiler can't understand run-time possibilities' 
         is adequate casting (not possible here) or reflection !
         [1] : Even Character, which is the boxed equivalent of 'char'. 'char'
               is a valid number type as a primitive but Character cannot be 
               casted to Number 
               Example : 
               int    a = 456; char b      = (char) a;      // VALID ! 
               Number a = 456; Character b = (Character) a; // FAILS !
               You will be hard-pressed to find a way to make the second cast
               pass easily (something like (Character) ((char) a.intValue()) )
               That's why QuickBoxedCaster#cast doesn't return a Number but 
               an Object, instead !
       - Are you sure that adequate casting isn't possible ?
         YES, I AM !
         Java primitive/boxed conversion is pure madness :
         -> Number to double, through method invocation : 
            COMPILER ERROR !
         -> Boxed primitive (even those implementing the Number interface) to
            double, through method invocation :
            WORKS ! But isn't generic... (Even Character -> double works !)
         -> Object variable containing a boxed primitive reference to double
            conversion, through method invocation :
            COMPILER ERROR !
         So you might be tempted to change the signature from
         double, double to Object, Object
         This will not trigger any compiler error and might seem the same
         as what reflection does.
         But then, you'll then get errors like :
         -> Assert.assertEquals fails : expected <0>, got <0>
         Because these two 0 are represented by different classes...
    */

    try {
      for (QuickBoxedCaster caster : casters) {
        output_value = caster.cast(long_value);
        this.check_method.invoke(this, output_value.getClass(), 
                                 expected_output_class, 
                                 output_value, long_value);
        output_value = caster.cast(char_value);
        this.check_method.invoke(this, output_value.getClass(), 
                            expected_output_class, output_value, char_value);
        output_value = caster.cast(character_value);
        this.check_method.invoke(this, output_value.getClass(), 
                            expected_output_class, output_value, 
                            character_value);
        output_value = caster.cast(byte_value);
        this.check_method.invoke(this, output_value.getClass(), 
                            expected_output_class, output_value, byte_value);
      }      
    } catch (Exception e) { throw new RuntimeException(e); }
  }

  public void testSoftOverflowCheck() {
    Object value;
    try {
      for (int index = 0; index < boxed_classes.length; index++) {
        Class boxed_class = boxed_classes[index];
        QuickBoxedCaster no_overflow_caster = 
          new QuickBoxedCaster(boxed_class, true);

        for (int sub_index = 0; sub_index < overflow_values.length;
             sub_index++) {
            value = no_overflow_caster.cast(overflow_values[sub_index]);
            this.check_method.invoke(this, value.getClass(), boxed_class,
                                     value, max_values[index][sub_index]);
          }
      }
    } catch (Exception e) { throw new RuntimeException(e); }
  }

  public void testStrictOverflowCheck() {
    Object value;
    boolean overflow_exception_triggered;

    for (int index = 0; index < boxed_classes.length; index++) {
      Class boxed_class = boxed_classes[index];
      QuickBoxedCaster strict_overflow_caster =
        new QuickBoxedCaster(boxed_class, true, true);
      for (int sub_index = 0; sub_index < overflow_values.length;
           sub_index++) {
        overflow_exception_triggered = false;
        try {
          strict_overflow_caster.cast(overflow_values[sub_index]);
        } catch (NumberFormatException e) {
          overflow_exception_triggered = true;
        }
        Assert.assertTrue(boxed_class.toString(), overflow_exception_triggered);
      }
    }
  }

  public void testDeniedParameters() {
    boolean illegal_argument_exception_caught;
    for (Class denied_class : this.denied_classes) {
      illegal_argument_exception_caught = false;
      try {
        new QuickBoxedCaster(denied_class);
      } catch (IllegalArgumentException e) {
        illegal_argument_exception_caught = true;
      }
      Assert.assertTrue(illegal_argument_exception_caught);
      try {
        new QuickBoxedCaster(denied_class, true);
      } catch (IllegalArgumentException e) {
        illegal_argument_exception_caught = true;
      }
      Assert.assertTrue(illegal_argument_exception_caught);
      try {
        new QuickBoxedCaster(denied_class, true, true);
      } catch (IllegalArgumentException e) {
        illegal_argument_exception_caught = true;
      }
      Assert.assertTrue(illegal_argument_exception_caught);
    }
  }

  // public void testCastNumber() {
  //   QuickBoxedCaster caster = new QuickBoxedCaster(long.class);
  //   Object return_value;
  //   for (Number number : this.numbers) {
  //     return_value = caster.cast(number);
  //     try {
  //       this.check_method.invoke(this,
  //                                Long.class, return_value.getClass(),
  //                                return_value, number);
  //     } catch (Exception e) { throw new RuntimeException(e); }
  //   }
  // }

  private void check_class_and_value(Class expected_class,
                                     Class provided_class,
                                     double expected_value,
                                     double provided_value) {
    check_class(expected_class, provided_class);
    check_value(expected_value, provided_value);
  }

  private void check_class(Class expected_class, Class provided_class) {
    Assert.assertTrue(expected_class == provided_class);
  }

  private void check_value(double expected_value, double provided_value)
  {
    Assert.assertEquals(expected_value, provided_value);
  }
}
